import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LanguageService } from 'src/app/services/language.service';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(public questionService: QuestionService, private router: Router, private fb: FormBuilder, public languageService: LanguageService) { }

  rechercheTitleForm = this.fb.group({
    title: ['', Validators.required],
  })

  rechercheLanguageForm = this.fb.group({
    language: ['', Validators.required],
  })

  onSubmitTitle() {
    this.questionService.findQuestionByTitle(this.rechercheTitleForm.value.title);
  }

  onSubmitLanguage() {
    this.languageService.getLanguagesByName(this.rechercheLanguageForm.value.language)
    this.questionService.findQuestionByLanguage(this.languageService.language$?.id);
  }

  questionsWithoutResponses() {
    this.questionService.findQuestionsWithoutResponses();
  }

  ngOnInit(): void {
    this.questionService.getQuestions()
    this.languageService.getLanguages()

  }

}
