import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-inscription-page',
  templateUrl: './inscription-page.component.html',
  styleUrls: ['./inscription-page.component.scss']
})
export class InscriptionPageComponent implements OnInit {

  constructor(public usersService: UsersService, private fb: FormBuilder) { }

  validator: String = "";

  userForm = this.fb.group({
    username: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
  })

  public onSubmit() {
    console.log(this.userForm.value)
    this.validator = this.usersService.createUser(this.userForm.value);
  }


  ngOnInit(): void {
  }

}
