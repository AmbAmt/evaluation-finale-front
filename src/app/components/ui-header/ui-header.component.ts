import { Component, Input, OnInit } from '@angular/core';
import { Users } from 'src/app/models/app.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-ui-header',
  templateUrl: './ui-header.component.html',
  styleUrls: ['./ui-header.component.scss']
})
export class UiHeaderComponent implements OnInit {

  constructor(public authenticationService: AuthenticationService, public userService: UsersService) { }

  @Input()
  user?: Users

  isLogged: string | null = sessionStorage.getItem("isLogged");

  ngOnInit(): void {
    if (this.authenticationService.isLogged && !this.userService.user$) {
      this.userService.reconstrucUserFromStorage();
    }
  }

}
