import { Injectable } from '@angular/core';
import axios from 'axios';
import { Tags } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class TagService {
  private apiUrl: string = "http://localhost:8080/tags";
  public tags$: Tags[] = [];
  public tag$?: Tags;

  constructor() { }

  public getTags() {
    axios.get(this.apiUrl)
      .then((response) => {
        this.tags$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }
}
