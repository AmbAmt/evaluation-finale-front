import { Component } from '@angular/core';
import { Router } from '@angular/router';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { AuthenticationService } from './services/authentication.service';
import { UsersService } from './services/users.service';
import { ErrorInterceptor } from './_helpers/error.interceptors';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'evaluation-finale-front';

  constructor(private authenticationService: AuthenticationService, private router: Router, private userService: UsersService) {
    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if (config.headers && config.method !== 'get') {
          config.headers['Authorization'] = this.authenticationService.getCurrentUserBasicAuthentication();
        }
        return config
      },
      (error: AxiosError) => {
        console.error('ERROR:', error)
        Promise.reject(error)
      }
    )

    const errorInterceptor = new ErrorInterceptor(authenticationService, router)
    errorInterceptor.errorInterceptor();
  }

  ngOnInit() {
    if (sessionStorage.getItem('isLogged') == 'true') {
      this.authenticationService.isLogged = true;
    }

    if (this.authenticationService.isLogged && !this.userService.user$) {
      this.userService.reconstrucUserFromStorage();
    }
  }
}
