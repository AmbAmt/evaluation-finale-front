import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiQuestionComponent } from './ui-question.component';

describe('UiQuestionComponent', () => {
  let component: UiQuestionComponent;
  let fixture: ComponentFixture<UiQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
