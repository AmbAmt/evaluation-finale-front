import { Injectable } from '@angular/core';
import axios from 'axios';
import { Responses } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  private apiUrl: string = "http://localhost:8080/responses";
  public responses$: Responses[] = [];
  public response$?: Responses;

  constructor() { }

  public getResponseByQuestionId(id: String) {
    axios.get(this.apiUrl + "?questionId=" + id)
      .then((res) => {
        this.responses$ = res.data;
        console.log(res.data)
      })
      .catch((error) => {
        console.log(error);
      });

  }

  public createResponse(questionId: String, data: any, email?: String) {
    axios.post("http://localhost:8080/questions/" + questionId, {
      "content": data.content,
      "userEmail": email,
    }).then((response) => {
      this.getResponseByQuestionId(questionId)
    }).catch(function (error) {
      console.log(error)
    })
    return "Création effectuée";
  }

  public voteResponse(idResponse: String | undefined, questionId: String) {
    axios.put(this.apiUrl + "/" + idResponse + "/vote")
      .then((response) => {
        this.getResponseByQuestionId(questionId)
      }).catch(function (error) {
        console.log(error)
      })
  }

  public getResponseByUserId(userId: String) {
    axios.get(this.apiUrl + "?userId=" + userId)
      .then((res) => {
        this.responses$ = res.data;
        console.log(res.data)
      })
      .catch((error) => {
        console.log(error);
      });
  }

  public isProblematic(responseId: String | undefined, questionId: String) {
    axios.put(this.apiUrl + "/" + responseId).then((response) => {
      this.getResponseByQuestionId(questionId)
    })
  }

  public removeResponse(responseId: String | undefined, questionId: String) {
    axios.delete(this.apiUrl + "/" + responseId).then((response) => {
      this.getResponseByQuestionId(questionId)
    })
  }
}
