
export interface Users {
    id: String,
    username: String,
    email: String,
    password: String,
}

export interface Questions {
    id: String,
    title: String,
    content: String,
    date: Date,
    language: Languages,
    user: Users,
    tag: Tags,
    problematic: Boolean,
}

export interface Responses {
    id: String,
    content: String,
    date: Date,
    vote: number,
    problematic: Boolean,
    question: Questions,
    user: Users,
}

export interface Tags {
    id: String,
    name: String
}

export interface Languages {
    id: String,
    name: String
}