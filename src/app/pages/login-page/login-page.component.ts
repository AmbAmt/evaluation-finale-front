import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  validator: String = ""

  constructor(private fb: FormBuilder, public authenticationService: AuthenticationService, public usersService: UsersService, public router: Router) { }

  ngOnInit(): void {
  }

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  })

  public onSubmit() {
    this.usersService.getUserByEmail(this.loginForm.value.email, this.loginForm.value.password)
      .then((user) => this.authenticationService.login(user))
    this.router.navigate(["/"])
  }
}
