import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiResponseComponent } from './ui-response.component';

describe('UiResponseComponent', () => {
  let component: UiResponseComponent;
  let fixture: ComponentFixture<UiResponseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiResponseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
