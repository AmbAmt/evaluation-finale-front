import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionAddPageComponent } from './question-add-page.component';

describe('QuestionAddPageComponent', () => {
  let component: QuestionAddPageComponent;
  let fixture: ComponentFixture<QuestionAddPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionAddPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
