import { Injectable } from '@angular/core';
import axios from 'axios';
import { Questions } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private apiUrl: string = "http://127.0.0.1:8080/questions";
  public questions$: Questions[] = [];
  public question$?: Questions;

  constructor() { }


  public getQuestions() {
    axios.get(this.apiUrl)
      .then((response) => {
        this.questions$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }

  public getQuestionById(id: String) {
    axios.get(this.apiUrl + "/" + id)
      .then((res) => {
        this.question$ = res.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  public createArticle(data: any, email?: String) {
    axios.post(this.apiUrl, {
      "title": data.title,
      "content": data.content,
      "tagName": data.tags,
      "language": data.language,
      "userEmail": email,
    }).then(function (response) {
      console.log(response)
    }).catch(function (error) {
      console.log(error)
    })
    return "Création effectuée";
  }

  public findQuestionByTitle(title: String) {
    axios.get(this.apiUrl + "?title=" + title).then((response) => {
      this.questions$ = response.data
    }).catch((error) => {
      console.log(error)
    })
  }

  public findQuestionByLanguage(languageId: String | undefined) {
    axios.get(this.apiUrl + "?languageId=" + languageId).then((response) => {
      this.questions$ = response.data
    }).catch((error) => {
      console.log(error)
    })
  }

  public findQuestionsWithoutResponses() {
    axios.get(this.apiUrl + "?noResponses")
      .then((response) => {
        this.questions$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }

  public getQuestionByUserId(userId: String) {
    axios.get(this.apiUrl + "?userId=" + userId)
      .then((res) => {
        this.questions$ = res.data;
        console.log(res.data)
      })
      .catch((error) => {
        console.log(error);
      });
  }

  public isProblematic(questionId: any) {
    axios.put(this.apiUrl + "/" + questionId).then((response) => {
      this.getQuestionById(questionId)
    })
  }

  public removeQuestion(questionId: any) {
    axios.delete(this.apiUrl + "/" + questionId).then((response) => {
      this.getQuestionById(questionId)
    })
  }

  public removeQuestionAndGetAll(questionId: any) {
    axios.delete(this.apiUrl + "/" + questionId).then((response) => {
      this.getQuestions()
    })
  }
}
