import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Questions, Responses } from 'src/app/models/app.model';
import { QuestionService } from 'src/app/services/question.service';
import { ResponseService } from 'src/app/services/response.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-question-detail-page',
  templateUrl: './question-detail-page.component.html',
  styleUrls: ['./question-detail-page.component.scss']
})
export class QuestionDetailPageComponent implements OnInit {

  @Input()
  public question?: Questions;

  public sessionStorageValue: String | null = sessionStorage.getItem('isLogged')

  constructor(private route: ActivatedRoute, public questionService: QuestionService, public responseService: ResponseService, private router: Router) { }

  isProblematic() {
    this.route.params.subscribe(params => {
      this.questionService.isProblematic(params['id'])
    })
  }

  clickOnDeleteButton() {
    this.route.params.subscribe(params => {
      this.questionService.removeQuestionAndGetAll(params['id'])
      this.router.navigate(["/"])
    })
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.questionService.getQuestionById(params['id'])
      this.responseService.getResponseByQuestionId(params['id'])
    })
  }

}
