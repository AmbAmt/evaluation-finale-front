import { Component, Input, OnInit, Output } from '@angular/core';
import { Underline } from 'lucide-angular';
import { Questions } from 'src/app/models/app.model';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-ui-question',
  templateUrl: './ui-question.component.html',
  styleUrls: ['./ui-question.component.scss']
})
export class UiQuestionComponent implements OnInit {

  @Input()
  public question?: Questions

  public sessionStorageValue: String | null = sessionStorage.getItem('isLogged')

  constructor(public questionService: QuestionService) { }

  isProblematic() {
    this.questionService.isProblematic(this.question?.id)
    location.reload();
  }

  clickOnDeleteButton() {
    this.questionService.removeQuestion(this.question?.id)
    location.reload();
  }

  ngOnInit(): void {
  }

}
