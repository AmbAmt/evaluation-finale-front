import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiQuestionComponent } from './components/ui-question/ui-question.component';
import { UiHeaderComponent } from './components/ui-header/ui-header.component';
import { QuestionAddPageComponent } from './pages/question-add-page/question-add-page.component';
import { QuestionDetailPageComponent } from './pages/question-detail-page/question-detail-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { InscriptionPageComponent } from './pages/inscription-page/inscription-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ResponseAddPageComponent } from './pages/response-add-page/response-add-page.component';
import { icons, LucideAngularModule } from 'lucide-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { UiResponseComponent } from './components/ui-response/ui-response.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';

@NgModule({
  declarations: [
    AppComponent,
    UiQuestionComponent,
    UiHeaderComponent,
    QuestionAddPageComponent,
    QuestionDetailPageComponent,
    HomePageComponent,
    InscriptionPageComponent,
    LoginPageComponent,
    ResponseAddPageComponent,
    UiResponseComponent,
    UserDetailPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick(icons),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
