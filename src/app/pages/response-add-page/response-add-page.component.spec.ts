import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponseAddPageComponent } from './response-add-page.component';

describe('ResponseAddPageComponent', () => {
  let component: ResponseAddPageComponent;
  let fixture: ComponentFixture<ResponseAddPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponseAddPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
