import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';
import { ResponseService } from 'src/app/services/response.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-response-add-page',
  templateUrl: './response-add-page.component.html',
  styleUrls: ['./response-add-page.component.scss']
})
export class ResponseAddPageComponent implements OnInit {


  constructor(public responseService: ResponseService, private fb: FormBuilder, public userService: UsersService, private router: Router, private route: ActivatedRoute) { }

  validator: String = "";

  responseForm = this.fb.group({
    content: ['', Validators.required],
  })

  public onSubmit() {
    let content = this.responseForm.value.content
    if (content.split(" ").length <= 100) {
      if (sessionStorage.getItem("current_user")) {
        this.route.params.subscribe(params => {
          this.validator = this.responseService.createResponse(params['id'], this.responseForm.value, this.userService.user$?.email);
          //location.reload();
        })
      } else {
        this.router.navigate(['/connexion'])
      }
    }
    else {
      window.alert("La réponse doit faire moins de 100 mots")
    }
  }
  ngOnInit(): void {
  }

}
