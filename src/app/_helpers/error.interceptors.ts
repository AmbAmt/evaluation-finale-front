import { Router } from "@angular/router";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import { AuthenticationService } from "../services/authentication.service";


export class ErrorInterceptor {

    constructor(private authenticationService: AuthenticationService, private router: Router) {

    }

    errorInterceptor = () => {
        axios.interceptors.response.use(
            response => {
                return response
            }, error => {
                console.log("Error =>" + error.response.status)
                if (error.response.status == 401) {
                    this.authenticationService.logout();
                    this.router.navigate(['/connexion'])
                }
                return Promise.reject(error);
            })
    }

}