import { Injectable } from '@angular/core';
import axios from 'axios';
import { Users } from '../models/app.model';
import { AuthenticationService } from './authentication.service';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiUrl: string = "http://localhost:8080/users"
  public user$?: Users;

  constructor() { }

  reconstrucUserFromStorage(): void {
    const currentUserPlain = sessionStorage.getItem(AuthenticationService.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      this.user$ = JSON.parse(currentUserPlain);
    }
  }

  public getUserByEmail(email: string, password: string) {
    return axios.get(this.apiUrl + "?email=" + email)
      .then((res) => {
        this.user$ = {
          id: res.data.id,
          username: res.data.username,
          //username: username,
          email: res.data.email,
          password: password
        }
        return this.user$;
      }
      );
  }

  public createUser(data: any) {
    axios.post(this.apiUrl, {
      "username": data.username,
      "email": data.email,
      "password": data.password
    }).then(function (response) {
      console.log(response)
    }).catch(function (error) {
      console.log(error)
    })

    return "Inscription effectuée"
  }
}
