import { Injectable } from '@angular/core';
import { Users } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public static SESSION_STORAGE_KEY = 'current_user'
  public isLogged: boolean = false

  constructor() { }

  login(user: Users | undefined) {
    sessionStorage.setItem(AuthenticationService.SESSION_STORAGE_KEY, JSON.stringify(user))
    sessionStorage.setItem('isLogged', 'true')
    this.isLogged = true
  }

  logout(): void {
    sessionStorage.removeItem(AuthenticationService.SESSION_STORAGE_KEY)
    sessionStorage.setItem('isLogged', 'false')
    this.isLogged = false
  }

  getCurrentUserBasicAuthentication(): string {
    let response: string = "";
    const currentUserPlain = sessionStorage.getItem(AuthenticationService.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      response = "Basic " + btoa(currentUser.username + ":" + currentUser.password)
    }
    return response;
  }
}
