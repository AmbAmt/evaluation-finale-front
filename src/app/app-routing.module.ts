import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { InscriptionPageComponent } from './pages/inscription-page/inscription-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { QuestionAddPageComponent } from './pages/question-add-page/question-add-page.component';
import { QuestionDetailPageComponent } from './pages/question-detail-page/question-detail-page.component';
import { ResponseAddPageComponent } from './pages/response-add-page/response-add-page.component';
import { UserDetailPageComponent } from './pages/user-detail-page/user-detail-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'connexion', component: LoginPageComponent },
  { path: 'inscription', component: InscriptionPageComponent },
  { path: 'question/:id', component: QuestionDetailPageComponent },
  { path: 'add', component: QuestionAddPageComponent },
  { path: 'user/:id', component: UserDetailPageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
