import { Injectable } from '@angular/core';
import axios from 'axios';
import { Languages } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private apiUrl: string = "http://localhost:8080/languages";
  public languages$: Languages[] = [];
  public language$?: Languages;

  constructor() { }

  public getLanguages() {
    axios.get(this.apiUrl)
      .then((response) => {
        this.languages$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }

  public getLanguagesByName(language: String) {
    axios.get(this.apiUrl + "?language=" + language)
      .then((response) => {
        this.language$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }
}
