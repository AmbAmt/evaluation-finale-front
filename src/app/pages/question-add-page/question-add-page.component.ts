import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LanguageService } from 'src/app/services/language.service';
import { QuestionService } from 'src/app/services/question.service';
import { TagService } from 'src/app/services/tag.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-question-add-page',
  templateUrl: './question-add-page.component.html',
  styleUrls: ['./question-add-page.component.scss']
})
export class QuestionAddPageComponent implements OnInit {

  validator: String = "";

  constructor(public questionService: QuestionService, private fb: FormBuilder, public userService: UsersService, public languageService: LanguageService, public tagService: TagService) { }

  questionForm = this.fb.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
    tags: ['', Validators.required],
    language: ['', Validators.required],

  })

  public onSubmit() {
    let title = this.questionForm.value.title
    if (title.split(" ").length <= 20 && title.charAt(title.length - 1) == '?') {
      this.validator = this.questionService.createArticle(this.questionForm.value, this.userService.user$?.email);
    }
    else {
      window.alert("Le titre doit faire vingt mots maximum et doit comporter un point d'interrogation")
    }

  }


  ngOnInit(): void {
    this.tagService.getTags()
    this.languageService.getLanguages()
    const currentUserPlain = sessionStorage.getItem("current_user")
  }

}
