import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Responses } from 'src/app/models/app.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ResponseService } from 'src/app/services/response.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-ui-response',
  templateUrl: './ui-response.component.html',
  styleUrls: ['./ui-response.component.scss']
})
export class UiResponseComponent implements OnInit {

  @Input()
  public response?: Responses
  public sessionStorageValue: String | null = sessionStorage.getItem('isLogged')

  constructor(public responseService: ResponseService, public userService: UsersService, public authentificationService: AuthenticationService, public route: ActivatedRoute) { }

  vote() {
    if (this.response?.user.email != this.userService.user$?.email) {
      this.route.params.subscribe(params => {
        this.responseService.voteResponse(this.response?.id, params['id'])
      })
    } else {
      window.alert("vous ne pouvez pas voter pour votre réponse")
    }
  }

  isProblematic() {
    this.route.params.subscribe(params => {
      this.responseService.isProblematic(this.response?.id, params['id'])
    })
    //location.reload();

  }


  clickOnDeleteButton() {
    this.route.params.subscribe(params => {
      this.responseService.removeResponse(this.response?.id, params['id'])
    })
    // location.reload()

  }
  ngOnInit(): void {
  }

}
